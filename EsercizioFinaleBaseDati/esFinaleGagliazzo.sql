CREATE DATABASE GestioneDevelopers;

USE GestioneDevelopers;

CREATE TABLE Sviluppatori(
	codice_fiscale varchar(16),
    password varchar(50),
    CONSTRAINT PK_Sviluppatori PRIMARY KEY(codice_fiscale)
);



CREATE TABLE ValutazioniSviluppatori(
	codice_fiscale_sviluppatore varchar(16),
    codice_fiscale_coordinatore varchar(16),
    voto int,
    check(voto>0 and voto<6),
    CONSTRAINT FK_Sviluppatore_ValutazioneSviluppatori FOREIGN KEY (codice_fiscale_sviluppatore) REFERENCES Sviluppatori(codice_fiscale),
	CONSTRAINT FK_Coordinatore_ValutazioneSviluppatori FOREIGN KEY (codice_fiscale_coordinatore) REFERENCES Sviluppatori(codice_fiscale),
    CONSTRAINT PK_ValutazioneSvlippatori PRIMARY KEY(codice_fiscale_sviluppatore,codice_fiscale_coordinatore)
);



CREATE TABLE Curriculum(
	id int NOT NULL AUTO_INCREMENT,
    tipologia varchar(20),
    codice_fiscale_sviluppatore varchar(16),
    CONSTRAINT FK_Sviluppatore_Curriculum FOREIGN KEY(codice_fiscale_sviluppatore) REFERENCES Sviluppatori(codice_fiscale),
    CONSTRAINT PK_Curriculum PRIMARY KEY (id)
);




CREATE TABLE InformazioniDiContatto(
	id int NOT NULL AUTO_INCREMENT,
    numero_telefono varchar(10),
    email varchar(60),
    codice_fiscale_sviluppatore varchar(16),
    CONSTRAINT FK_Sviluppatore_InformazioneDiContatto FOREIGN KEY(codice_fiscale_sviluppatore) REFERENCES Sviluppatori(codice_fiscale),
    CONSTRAINT PK_InformazioneDiContatto PRIMARY KEY (id)
);




CREATE TABLE InformazioniAnagrafiche(
	id int NOT NULL AUTO_INCREMENT,
    nome varchar(30),
    cognome varchar(40),
    data_nascita date,
    codice_fiscale_sviluppatore varchar(16),
    CONSTRAINT FK_Sviluppatore_InformazioniAnagrafiche FOREIGN KEY(codice_fiscale_sviluppatore) REFERENCES Sviluppatori(codice_fiscale),
    CONSTRAINT PK_InformazioniAnagrafiche PRIMARY KEY (id)
);



CREATE TABLE Progetti(
	id int NOT NULL AUTO_INCREMENT,
    nome varchar(50),
	codice_fiscale_sviluppatore varchar(16),
    CONSTRAINT FK_Sviluppatore_Progetti FOREIGN KEY(codice_fiscale_sviluppatore) REFERENCES Sviluppatori(codice_fiscale),
    CONSTRAINT PK_Progetti PRIMARY KEY (id)
);



CREATE TABLE Messaggi(
	id int NOT NULL AUTO_INCREMENT,
    visibilita varchar(10),
    contenuto varchar(255),
    codice_fiscale_sviluppatore varchar(16),
    id_progetto int,
    CONSTRAINT PK_Messaggi PRIMARY KEY (id),
    CONSTRAINT FK_Sviluppatore_Messaggi FOREIGN KEY (codice_fiscale_sviluppatore) REFERENCES Sviluppatori(codice_fiscale),
	CONSTRAINT FK_Progetto_Messaggi FOREIGN KEY(id_progetto) REFERENCES Progetti(id),
    CHECK(visibilita="Pubblica" or visibilita="Privata")
);



CREATE TABLE Tipologia(
	id int NOT NULL AUTO_INCREMENT,
    nome varchar(60),
	CONSTRAINT PK_Tipologia PRIMARY KEY (id)
);




CREATE TABLE Task(
	id int NOT NULL AUTO_INCREMENT,
    descrizione varchar(255),
    numero_collaboratori_richiesti int,
    data_fine date,
    stato varchar(10),
    id_progetto int,
    id_tipologia int,
    CONSTRAINT PK_Task PRIMARY KEY(id),
    CONSTRAINT FK_Progetto_Task FOREIGN KEY(id_progetto) REFERENCES Progetti(id),
    CONSTRAINT FK_Tipologia_Task FOREIGN KEY(id_tipologia) REFERENCES Tipologia(id),
    CHECK(stato="Aperto" or Stato="Chiuso")
);




CREATE TABLE FileD(
	id int NOT NULL AUTO_INCREMENT,
    nome varchar(100),
    tipologia varchar(5),
    contenuto varchar(255),
    id_task int,
    CONSTRAINT PK_File PRIMARY KEY(id),
    CONSTRAINT FK_Task_File FOREIGN KEY(id_task) REFERENCES Task(id)
);



CREATE TABLE ModificheFile(
	id int NOT NULL AUTO_INCREMENT,
	codice_fiscale_sviluppatore varchar(16),
    id_file int,
    data_modifica date,
    CONSTRAINT FK_Sviluppatore_ModificheFile FOREIGN KEY (codice_fiscale_sviluppatore) REFERENCES Sviluppatori (codice_fiscale),
    CONSTRAINT FK_File_ModificheFile FOREIGN KEY(id_file) REFERENCES FileD(id),
    CONSTRAINT PK_ModificheFile PRIMARY KEY(id)

);


CREATE TABLE SviluppoTask(
	id_task int,
    codice_fiscale_sviluppatore varchar(16),
	CONSTRAINT FK_Sviluppatore_SviluppoTask FOREIGN KEY (codice_fiscale_sviluppatore) REFERENCES Sviluppatori (codice_fiscale),
    CONSTRAINT FK_Task_SviluppoTask FOREIGN KEY(id_task) REFERENCES Task(id),
    CONSTRAINT PK_SviluppoTask PRIMARY KEY(id_task,codice_fiscale_sviluppatore)
);



CREATE TABLE Skill(
	id int NOT NULL AUTO_INCREMENT,
    nome varchar(60),
    livello_competenza int,
    id_tipologia int,
	CONSTRAINT FK_Tipologia_Skill FOREIGN KEY (id_tipologia) REFERENCES Tipologia(id),
    CONSTRAINT PK_Skill PRIMARY KEY(id),
    CHECK(0<livello_competenza<11)
);




CREATE TABLE SkillSviluppatori(
	codice_fiscale_sviluppatore varchar(16),
    id_skill int,
	CONSTRAINT FK_Sviluppatore_SkillSviluppatori FOREIGN KEY (codice_fiscale_sviluppatore) REFERENCES Sviluppatori (codice_fiscale),
    CONSTRAINT FK_Skill_SkillSviluppatori FOREIGN KEY(id_skill) REFERENCES Skill(id),
    CONSTRAINT PK_SkillSviluppatori PRIMARY KEY(id_skill,codice_fiscale_sviluppatore)
);



CREATE TABLE SkillRichiesta(
	id_task int,
    id_skill int,
	CONSTRAINT FK_Task_SkillRichiesta FOREIGN KEY (id_task) REFERENCES Task(id),
    CONSTRAINT FK_Skill_SkillRichiesta FOREIGN KEY(id_skill) REFERENCES Skill(id),
    CONSTRAINT PK_SkillRichiesta PRIMARY KEY(id_task,id_skill)
);


/*Inserimenti vari*/


INSERT INTO Sviluppatori VALUES
('GGLLSN04', 'ciaociao12'),
('PPFF97', 'verderosso100'),
('RBBB85', '44gatti');



INSERT INTO Curriculum (tipologia, codice_fiscale_sviluppatore) VALUES
('PDF', 'GGLLSN04'),
('PDF', 'PPFF97'),
('Testuale', 'RBBB85');



INSERT INTO InformazioniDiContatto (numero_telefono, email, codice_fiscale_sviluppatore) VALUES
('1234567890', 'AG@gmail.com', 'GGLLSN04'),
('2345678901', 'PippoFerrari@gmail.com', 'PPFF97'),
('3456789012', 'Roberto85Bianchi@outlook.com', 'RBBB85');



INSERT INTO InformazioniAnagrafiche (nome, cognome, data_nascita, codice_fiscale_sviluppatore) VALUES
('Alessandro', 'Gagliazzo', '2004-11-29', 'GGLLSN04'),
('Pippo', 'Ferrari', '1997-05-05', 'PPFF97'),
('Roberto', 'Bianchi', '1985-12-12', 'RBBB85');



INSERT INTO Progetti (nome, codice_fiscale_sviluppatore) VALUES
('Fifa 29', 'RBBB85'),
('Sito bellissimo', 'PPFF97');




INSERT INTO Tipologia (nome) VALUES
('Web development'),
('Mobile development'),
('Data Science');


INSERT INTO Task (descrizione, numero_collaboratori_richiesti, data_fine, stato, id_progetto, id_tipologia) VALUES
('Sviluppo front-end', 2, '2024-06-30', 'Aperto', 2, 1),
('Implementazione feature X', 3, '2024-07-15', 'Aperto', 1, 2),
('Analisi dei dati', 1, '2024-06-30', 'Chiuso', 1, 3);


INSERT INTO SviluppoTask (id_task, codice_fiscale_sviluppatore) VALUES
(1, 'GGLLSN04'),
(1, 'RBBB85'),
(2, 'RBBB85');



INSERT INTO Messaggi (visibilita, contenuto, codice_fiscale_sviluppatore, id_progetto) VALUES
('Pubblica', 'Ho bisogno di una mano con la task', 'RBBB85', 1),
('Pubblica', 'Ti aiuto io!', 'GGLLSN04', 1),
('Pubblica', 'Ho concluso la task', 'PPFF97', 2);



INSERT INTO FileD (nome, tipologia, contenuto, id_task) VALUES
('index.html', 'HTML', '<html>...</html>', 1),
('app.js', 'JS', 'console.log("Hello World!");', 1),
('report.pdf', 'PDF', '...', 2);


INSERT INTO ModificheFile (codice_fiscale_sviluppatore, id_file, data_modifica) VALUES
('GGLLSN04', 4, '2024-06-01'),
('RBBB85', 5, '2024-06-10'),
('PPFF97', 5, '2024-06-05');


INSERT INTO ModificheFile (codice_fiscale_sviluppatore, id_file, data_modifica) VALUES
('PPFF97', 6, '2024-04-10');

INSERT INTO ModificheFile (codice_fiscale_sviluppatore, id_file, data_modifica) VALUES
('PPFF97', 6, '2022-04-20');

INSERT INTO Skill (nome, livello_competenza, id_tipologia) VALUES
('HTML', 8, 1),
('React Native', 7, 2),
('Python', 9, 3);



INSERT INTO SkillSviluppatori (codice_fiscale_sviluppatore, id_skill) VALUES
('GGLLSN04', 1),
('RBBB85', 2),
('PPFF97', 3);



INSERT INTO SkillRichiesta (id_task, id_skill) VALUES
(1, 1),
(2, 2),
(3, 3);



/*Operazioni da realizzare*/

/*1*/

SELECT Progetti.nome,Task.stato
FROM PROGETTI
INNER JOIN TASK ON PROGETTI.id=TASK.id_progetto
WHERE stato='Aperto';


/*2*/

INSERT INTO Progetti (Nome,Codice_Fiscale_Sviluppatore) VALUES 
('Database gestione progetto pizzeria', 'GGLLSN04');


INSERT INTO Task (descrizione, numero_collaboratori_richiesti, data_fine, stato, id_progetto, id_tipologia) VALUES
('Creazione database pizzeria',2,'2024-06-06','Aperto',3,3);




/*3*/
SELECT Progetti.nome,COUNT(ModificheFile.id_file) as Numero_Modifiche
FROM PROGETTI
INNER JOIN TASK ON PROGETTI.ID=TASK.id_progetto
INNER JOIN FILED ON FILED.id_task=TASK.ID
INNER JOIN MODIFICHEFILE ON FILED.id=MODIFICHEFILE.id_file
GROUP BY PROGETTI.id
ORDER BY Numero_Modifiche desc;


/*4*/

SELECT Progetti.nome,MAX(ModificheFile.data_modifica) AS Ultima_Modifica
FROM PROGETTI
INNER JOIN TASK ON PROGETTI.ID=TASK.id_progetto
INNER JOIN FILED ON FILED.id_task=TASK.ID
INNER JOIN MODIFICHEFILE ON FILED.id=MODIFICHEFILE.id_file
GROUP BY Progetti.id
HAVING DATEDIFF(NOW(),Ultima_Modifica)>365;




/*5*/

SELECT InformazioniAnagrafiche.nome,InformazioniAnagrafiche.cognome,Skill.nome,Skill.Livello_competenza
FROM InformazioniAnagrafiche
INNER JOIN Sviluppatori ON Sviluppatori.codice_fiscale=InformazioniAnagrafiche.codice_fiscale_sviluppatore
INNER JOIN SKILLSVILUPPATORI ON SKILLSVILUPPATORI.codice_fiscale_sviluppatore=Sviluppatori.codice_fiscale
INNER JOIN SKILL ON SKILL.id=SKILLSVILUPPATORI.id_skill
WHERE Skill.nome="Python" and Skill.livello_competenza>2;



/*6*/

SELECT Progetti.nome
FROM Progetti
INNER JOIN Task ON Task.id_progetto=Progetti.id
WHERE Progetti.nome like '%bellissimo%' or Task.descrizione like '%bellissimo%';




/*7*/
INSERT INTO Sviluppatori (codice_fiscale, password) VALUES 
("LLRR01","gattibelli1010");


INSERT INTO Curriculum (tipologia, codice_fiscale_sviluppatore) VALUES
('PDF', 'LLRR01');



INSERT INTO InformazioniDiContatto (numero_telefono, email, codice_fiscale_sviluppatore) VALUES
('5556677881', 'LorenzoRossi@gmail.com', 'LLRR01');



INSERT INTO InformazioniAnagrafiche (nome, cognome, data_nascita, codice_fiscale_sviluppatore) VALUES
('Lorenzo', 'Rossi', '2001-11-18', 'LLRR01');



/*8*/
INSERT INTO SkillSviluppatori (codice_fiscale_sviluppatore,id_skill) VALUES 
("GGLLSN04",3);



/*9*/
INSERT INTO ValutazioniSviluppatori (codice_fiscale_sviluppatore, codice_fiscale_coordinatore, voto) VALUES
("GGLLSN04","RBBB85",4);

select * from sviluppotask;

/*10*/
SELECT InformazioniAnagrafiche.nome,InformazioniAnagrafiche.cognome,Progetti.nome
FROM InformazioniAnagrafiche
INNER JOIN Sviluppatori ON Sviluppatori.codice_fiscale=InformazioniAnagrafiche.codice_fiscale_sviluppatore
INNER JOIN SviluppoTask on SviluppoTask.codice_fiscale_sviluppatore=Sviluppatori.codice_fiscale
INNER JOIN Task ON Task.id=SviluppoTask.id_task
INNER JOIN Progetti ON Progetti.id=Task.id_progetto
ORDER BY InformazioniAnagrafiche.nome;



/*11*/
UPDATE Task
SET data_fine='2024-11-10'
WHERE Task.id=4;



/*12*/
DELETE FROM SviluppoTask
WHERE SviluppoTask.id_task=2 AND codice_fiscale_sviluppatore="RBBB85";



/*13*/
SELECT FileD.nome, ModificheFile.data_modifica
FROM ModificheFile
INNER JOIN FileD ON FileD.id=ModificheFile.id_file
WHERE FileD.id=5
ORDER BY data_modifica desc
LIMIT 1 OFFSET 1;



/*14*/
SELECT FileD.nome,InformazioniAnagrafiche.nome,InformazioniAnagrafiche.cognome,ModificheFile.data_modifica
FROM InformazioniAnagrafiche
INNER JOIN Sviluppatori ON Sviluppatori.codice_fiscale=InformazioniAnagrafiche.codice_fiscale_sviluppatore
INNER JOIN ModificheFile ON Sviluppatori.codice_fiscale=ModificheFile.codice_fiscale_sviluppatore
INNER JOIN FILED ON FileD.id=ModificheFile.id_file
ORDER BY FileD.nome;